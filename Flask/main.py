from flask import Flask, request, redirect, url_for, escape, session, g, redirect, url_for, \
    abort, render_template, flash, jsonify
import os, sys
import ConfigParser
import imp
from multiprocessing import Pool, Process
import time
import sqlite3

APP_FLASK = os.path.dirname(os.path.abspath(__file__))
APP_CHECKER = os.path.join(os.path.dirname(APP_FLASK), 'MainProgram')
APP_CONFIG = os.path.join(APP_CHECKER, 'config.txt')

APP_STATIC = os.path.join(APP_FLASK, 'static')
APP_RESULTS = os.path.join(APP_CHECKER, 'ServiceStatus')
APP_SERVICES = os.path.join(APP_CHECKER, 'Services')
APP_SSL = os.path.join(APP_FLASK, 'ssl')

sys.path.append(APP_CHECKER)
from MainProgram import *

checker = MainProgram()
# from OpenSSL import SSL

# DEBUG = True
DATABASE = 'flask.db'
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'admin'

app = Flask(__name__)
app.config.from_object(__name__)

# context = SSL.Context(SSL.SSLv23_METHOD)
# context.use_privatekey_file(os.path.join(APP_SSL, 'ssl.key'))
# context.use_certificate_file(os.path.join(APP_SSL, 'ssl.cert'))


class Result:
    def __init__(self, name, status, last_change):
        self.name = name
        self.status = status
        self.last_change = last_change


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_db()
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


@app.route('/')
def show_results():
    global update_time
    results = []
    error = None

    if os.path.exists(APP_RESULTS):
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()
        update_time = raw_results[0].split(".")[0]
        del raw_results[0]

        for raw_result in raw_results:
            if raw_result:
                result = raw_result.split(": ")
                result[2] = result[2].split(".")[0]
                results.append(Result(result[0], result[1], result[2]))
    else:
        error = "Could not find file with results."

    return render_template('show_results.html', check_time=update_time, results=results, error=error)


@app.route('/get_results')
def get_results():
    results = []
    if os.path.exists(APP_RESULTS):
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()
        update_time = raw_results[1]
        del raw_results[0]
        print update_time
        for raw_result in raw_results:
            splited_result = raw_result.split(": ")
            result = {'name': splited_result[0], 'status': splited_result[1], 'last_update': splited_result[2]}
            results.append(result)
    else:
        results = "Could not find file with results."
    return jsonify(results=results)


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        user = query_db('select username from entries where username = ?', [request.form['username']], one=True)
        if user is None:
            error = 'Invalid credentials'
        elif request.form['hashed'] != \
                query_db('select password from entries where username = ?', [request.form['username']], one=True)[0]:
            error = 'Invalid credentials'

            # if request.form['username'] != app.config['USERNAME']:
            # error = 'Invalid username'
            # elif request.form['password'] != app.config['PASSWORD']:
            # error = 'Invalid password'
        else:
            session['logged_in'] = True
            session['username'] = request.form['username']
            return redirect(url_for('show_results'))
    return render_template('login.html', error=error)

@app.route('/change_password', methods=['GET', 'POST'])
def change_password():
    error = None
    if 'logged_in' in session:
        if request.method == 'POST':
            get_db().execute('update entries set password = ? where username = ?', (request.form['hashed'], session['username']))
            get_db().commit()
            return redirect(url_for('show_results'))
        if request.method == 'GET':
            return render_template('change_password.html', error=error)
    return redirect(url_for('show_results'))

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('username', None)
    flash('You have been logged out.')
    return redirect(url_for('show_results'))


@app.route('/settings', methods=['GET', 'POST'])
def settings(request=request, error=None):
    if 'logged_in' in session:
        try:
            if request.method == 'GET':
                return render_settings()
            if request.method == 'POST':
                if not (request.form['upload'].isdigit() and request.form['queue'].isdigit()
                    and request.form['request'].isdigit() and request.form['mail'].isdigit()
                    and request.form['interval'].isdigit()):
                    raise Exception("One of the inputs is not a number!")

                config = ConfigParser.ConfigParser()
                config.read(APP_CONFIG)
                
                config.set("TIMEOUT", 'upload', request.form['upload'])
                config.set("TIMEOUT", 'queue', request.form['queue'])
                config.set("TIMEOUT", 'request', request.form['request'])
                config.set("TIMEOUT", 'mail', request.form['mail'])
                config.set("TIMEOUT", 'interval', request.form['interval'])
                config.set("MAIL_ADDR", 'mail', request.form['mail_addr'])

                with open(APP_CONFIG, 'wb') as configfile:
                    config.write(configfile)

                flash('Settings have been updated.')
                return redirect(url_for("settings"))
        except Exception,e:
            return render_settings(e)
    return redirect(url_for('login'))

def render_settings(error=None):
    config = ConfigParser.ConfigParser()
    config.read(APP_CONFIG)

    upload = config.get("TIMEOUT", 'upload')
    queue = config.get("TIMEOUT", 'queue')
    request = config.get("TIMEOUT", 'request')
    mail = config.get("TIMEOUT", 'mail')
    interval = config.get("TIMEOUT", 'interval')
    mail_addr = config.get("MAIL_ADDR", 'mail')

    return render_template('settings.html', upload=upload,
                                       queue=queue, request=request, mail=mail, interval=interval, error=error, mail_addr=mail_addr)

@app.route('/config')
@app.route('/config/<name>', methods=['GET', 'POST'])
def configure(name=''):
    error = None
    APP_UPDATE = os.path.join(APP_SERVICES, name)
    routes = []
    check = "yes"
    if os.path.exists(APP_RESULTS):
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()
        update_time = raw_results[1]
        del raw_results[0]
        for raw_result in raw_results:
            splited_result = raw_result.split(": ")
            result = splited_result[0]
            if result != 'UPLOAD' and result != name:
                routes.append(result)
    else:
        results = "Could not find file with results."

    if 'logged_in' in session and name != '':
        config = ConfigParser.ConfigParser()
        config.read(APP_CONFIG)
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()

        if not os.path.isdir(os.path.join(APP_SERVICES, name)):
            error = name + " is not avilable!"
        if request.method == 'GET':
            if config.has_option("INPUT", name):
                expectedFile = config.get("INPUT", name).decode('utf-8')
            else:
                expectedFile = " "
                check = "no"
            service = config.get("SERVICES", name).decode('utf-8')
	
            return render_template('configuration.html', name=name, service=service, routes=routes, expectedFile=expectedFile, check=check)
          
        if request.method == 'POST':
            if request.form['enable'] == 'on':
                if request.form['input']:
                    config.set("SERVICES", name, request.form['serv'])
                    config.set("INPUT", name, request.form['input'])
                    config.set("ROUTE", name, request.form['route'])
                    with open(APP_CONFIG, 'wb') as configfile:
                        config.write(configfile)
                elif request.form['route'] != 'NONE' and not request.form['input']:
                    config.set("SERVICES", name, request.form['serv'])
                    config.set("ROUTE", name, request.form['route'])
                    with open(APP_CONFIG, 'wb') as configfile:
                        config.write(configfile)
                elif request.form['route'] == 'NONE' and not request.form['input']:
                    flash('You must to change route or add text in the input area.', "error")
                    return render_template('configuration.html', name=name, error=error, routes=routes)
            elif request.form['enable'] == 'off':
                config.set("SERVICES", name, request.form['serv'])
                with open(APP_CONFIG, 'wb') as configfile:
                    config.write(configfile)
                    # return redirect(url_for('show_results'))
            flash('Configuration has been updated.')
            return redirect(url_for("show_results"))
    return redirect(url_for('login'))



def removeServiceChecker(name):
    checker.remove(name)
    print "Deleting " + name


def pingChecker(name):
    # checker.ping(name)

    try:
        print "Pinging " + name
        req_timeout = float(timeoutsLoader.getTimeout('request'))
        print req_timeout
        newProcess = Process(target=checker.ping, args=(name,))
        newProcess.daemon = False
        newProcess.start()

        newProcess.join(req_timeout)

        if newProcess.is_alive():
            
            newProcess.terminate()
            newProcess.join()
            return -1;
        else:
            return 0;
            
    except Exception,e:
        print str(e)
        pass

def startChecker():
    checker.start()


@app.route('/ping')
@app.route('/ping/<name>')
def ping(name):
    error = None
    if 'logged_in' in session:
        if pingChecker(name) == 0:
            flash('You have pinged successfully: ' + name)
        else:
            flash(name + ' has not responded in given time.')



            # flash('You have pinged successfully: ' + name)
            # except multiprocessing.TimeoutError:
            #     # TODO: Rise an error
            #     flash(name + ' has not responded in given time.')

    return redirect(url_for('show_results'))


@app.route('/remove')
@app.route('/remove/<name>')
def remove(name):
    error = None
    if 'logged_in' in session:
        try:
            req_timeout = float(timeoutsLoader.getTimeout('request'))
            print req_timeout
            newProcess = Process(target=removeServiceChecker, args=(name,))
            newProcess.daemon = False
            newProcess.start()

            newProcess.join(req_timeout)

            if newProcess.is_alive():
                flash(name + ' has not responded in given time.')
                newProcess.terminate()
                newProcess.join()
            else:
                flash('You have removed successfully: ' + name)

        except Exception, e:
            print str(e)
            pass


            # flash('You have pinged successfully: ' + name)
            # except multiprocessing.TimeoutError:
            #     # TODO: Rise an error
            #     flash(name + ' has not responded in given time.')
    return redirect(url_for('show_results'))


@app.route('/update')
@app.route('/update/<name>', methods=['GET', 'POST'])
def update(name):
    error = None
    APP_UPDATE = os.path.join(APP_SERVICES, name)
    if 'logged_in' in session and os.path.isfile(os.path.join(APP_UPDATE, 'WY.file')):
        try:
            PATH_WY_FILE = os.path.join(APP_UPDATE, 'WY.file')
            expectedFile = open(PATH_WY_FILE).read().decode('utf-8')
            print type(expectedFile)
            if os.path.isfile(os.path.join(APP_UPDATE, 'WY_new.file')):
                PATH_NEW_WY_FILE = os.path.join(APP_UPDATE, 'WY_new.file')
                recivedFile = open(PATH_NEW_WY_FILE).read().decode('utf-8')
            if not os.path.isdir(os.path.join(APP_SERVICES, name)):
                error = name + " is not configured!"

            if request.method == 'POST' and os.path.isfile(PATH_NEW_WY_FILE):
                if request.form['submit'] == 'update':
                    odp = request.form['recivedFile']
                    tmp = open(PATH_WY_FILE, 'w')
                    try:
                        tmp.write(odp.replace('\r', ''))
                    finally:
                        tmp.close()

                    os.remove(PATH_NEW_WY_FILE)
                    pingChecker(name)
                    flash('Updated has been successfull.')
            if request.method == 'GET':
                return render_template('update.html', name=name, error=error, expectedFile=expectedFile, recivedFile=recivedFile)
        except Exception,e:
            print str(e)
            pass
    return redirect(url_for('show_results'))


@app.route('/add', methods=['GET', 'POST'])
def add():
    error = None
    routes = []
    check = "yes"
    if os.path.exists(APP_RESULTS):
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()
        update_time = raw_results[1]
        del raw_results[0]
        for raw_result in raw_results:
            splited_result = raw_result.split(": ")
            result = splited_result[0]
            if (result != 'UPLOAD'):
                routes.append(result)
    else:
        results = "Could not find file with results."
    if 'logged_in' in session:
        config = ConfigParser.ConfigParser()
        config.read(APP_CONFIG)
        with open(APP_RESULTS) as f:
            raw_data = f.read()
        raw_results = raw_data.splitlines()

        if request.method == 'GET':
            return render_template('add.html', routes=routes)

        if request.method == 'POST':
            for raw_result in raw_results:
                splited_result = raw_result.split(": ")
                result = splited_result[0]
                if result == request.form['name']:
                    check = "no"
            if request.form['input']:
                if check == "yes": 
                    config.set("SERVICES", request.form['name'], request.form['serv'])
                    config.set("INPUT", request.form['name'], request.form['input'])
                    config.set("ROUTE", request.form['name'], request.form['route'])
                    if request.form['parameters']:
                        config.set("PARAMETERS", request.form['name'], request.form['parameters'])
                    with open(APP_CONFIG, 'wb') as configfile:
                         config.write(configfile)
                else:
                    flash('Change name.')
                    return render_template('add.html', error=error, routes=routes)
            pingChecker(request.form['name'])
            flash('Configuration has been updated.')
    return redirect(url_for('show_results'))


if __name__ == '__main__':
    p = Process(target=startChecker)
    p.start()

    # context = ('ssl/ssl.cert', 'ssl/ssl.key')
    app.run(
        host="0.0.0.0",
        port=int("5000"),
        # ssl_context=context,
        threaded=True
    )

    p.join()
