__author__ = 'konrad'

from Service import *

timeoutsLoader = TimeoutsLoader()

APP_MAIN = os.path.dirname(os.path.abspath(__file__))
APP_SERVICES = os.path.join(APP_MAIN, 'Services')
APP_SERVICESTATUS = os.path.join(APP_MAIN, 'ServiceStatus')
APP_CONFIG = os.path.join(APP_MAIN, 'config.txt')


def config():
    print("Configuring...")
    file = open(APP_SERVICESTATUS, 'w')
    dt = datetime.datetime.now()
    file.write(str(dt) + "\n")
    file.close()

    configuration = ConfigParser.ConfigParser()
    configuration.read(APP_CONFIG)
    listSections = configuration.sections()
    listOfObjects = []

    for section in listSections:
        listItems = configuration.items(section)
        if section == "SERVICES":
            for name, value in listItems:
                if value == "ON":
                    newObject = Service(name)
                    try:
                        newObject.parameters = configuration.get("PARAMETERS", newObject.name)
                    except:
                        newObject.parameters = None

                    try:
                        newObject.routeName = configuration.get("ROUTE", newObject.name)
                    except:
                        newObject.routeName = None

                    listOfObjects.append(newObject)

                    pathDir = os.path.join(APP_SERVICES, newObject.name)
                    if not os.path.isdir(pathDir):
                        os.mkdir(pathDir)
                    try:
                        contentFile = configuration.get("INPUT", newObject.name)
                        if os.path.exists(pathDir + '/WE.file.org'):
                            with open(pathDir + '/WE.file.org', "w") as defaultFile:
                                defaultFile.write(contentFile)
                        else:
                            with open(pathDir + '/WE.file.org', 'a') as defaultFile:
                                defaultFile.write(contentFile)

                        data = newObject.upload(pathDir + '/WE.file.org')
                        with open(pathDir + '/WE.file', "w") as retUpload:
                            retUpload.write(data)
                        checkService(newObject)

                    except:
                        try:
                            configRoute(newObject)
                            checkService(newObject)
                        except:
                            listOfObjects.remove(newObject)
                            writeLog(newObject.name, "Service doesn't have input data or route data")
                            writeStatus(newObject.name, ": WHITE")
                            
                elif value == "OFF":
                    writeStatus(name, ": WHITE")

    return listOfObjects


def getServicesList():
    configuration = ConfigParser.ConfigParser()
    configuration.read(APP_CONFIG)
    listSections = configuration.sections()
    listOfObjects = []

    for section in listSections:
        listItems = configuration.items(section)
        if section == "SERVICES":
            for name, value in listItems:
                if value == "ON":
                    newObject = Service(name)
                    try:
                        newObject.parameters = configuration.get("PARAMETERS", newObject.name)
                    except:
                        newObject.parameters = None

                    try:
                        newObject.routeName = configuration.get("ROUTE", newObject.name)
                    except:
                        newObject.routeName = None
                    listOfObjects.append(newObject)
    return listOfObjects


def configNewService(name):
    configuration = ConfigParser.ConfigParser()
    configuration.read(APP_CONFIG)
    newObject = Service(name)
    try:
        newObject.parameters = configuration.get("PARAMETERS", newObject.name)
    except:
        newObject.parameters = None

    try:
        newObject.routeName = configuration.get("ROUTE", newObject.name)
    except:
        newObject.routeName = None

    pathDir = os.path.join(APP_SERVICES, newObject.name)
    if not os.path.isdir(pathDir):
        os.mkdir(pathDir)
    try:
        contentFile = configuration.get("INPUT", newObject.name)
        if os.path.exists(pathDir + '/WE.file.org'):
            with open(pathDir + '/WE.file.org', "w") as defaultFile:
                defaultFile.write(contentFile)
        else:
            with open(pathDir + '/WE.file.org', 'a') as defaultFile:
                defaultFile.write(contentFile)

        data = newObject.upload(pathDir + '/WE.file.org')
        with open(pathDir + '/WE.file', "w") as retUpload:
            retUpload.write(data)
        checkService(newObject)
    except:
        configRoute(newObject)
        checkService(newObject)
    return newObject
