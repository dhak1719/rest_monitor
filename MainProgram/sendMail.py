#!/usr/bin/python 
# -*- coding: utf-8 -*-

import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

import datetime
import time
import ConfigParser
APP_MAIN = os.path.dirname(os.path.abspath(__file__))
APP_SERVICESTATUS = os.path.join(APP_MAIN, 'ServiceStatus')
APP_CONFIG = os.path.join(APP_MAIN, 'config.txt')

mailSent = []; 

def send_warning_status(SERVICE_NAME, RECIPIENTS):
            text = "%s is malfunctioning!" % SERVICE_NAME
            html = """\
<html>
<head>
<style>
* {
	margin: 0;
	padding: 0;
	}
 
body {
	}
	
p {
	position: relative;
	top: 50%;
	transform: translateY(-50%);

	float: left;
	text-align: center;
	font-family:courier;
    font-size:26pt;
	font-weight: 500;
	padding: 20% 5% 20% 5%;
	}
	
img {
	}	
	
#service {
	float: left;
	height: 307px;
	width: 410px;
	}
	
#bg {
	margin: 30px 30px;
	width: 800px;
	background: #ffeeaf;
	
	-webkit-box-shadow: 0px 0px 15px 5px rgba(165,105,0,0.75);
	-moz-box-shadow: 0px 0px 15px 5px rgba(165,105,0,0.75);
	box-shadow: 0px 0px 15px 5px rgba(165,105,0,0.75);
}
</style>
</head>
<body>
<div id="bg">
<div id="service"><p><b>""" + SERVICE_NAME + """</b> is malfunctioning!</p></div>
<img alt="Embedded image" src="cid:sign.png" />
</div>
</body>
</html>
"""
            send_email("Warning!", text, html, RECIPIENTS);
            return;

def send_email(SUBJECT, PLAIN_TXT, HTML_MSG, RECIPIENTS):
            FROM = "rest.monitor"
            msgRoot = MIMEMultipart('related')
            msgRoot['Subject'] = SUBJECT
            msgRoot['From'] = FROM
            msgRoot['To'] = ', '.join(RECIPIENTS)
            msgAlternative = MIMEMultipart('alternative')
            msgRoot.attach(msgAlternative)

            czysty_tekst = MIMEText(PLAIN_TXT, 'plain', 'utf-8')
            czesc_html = MIMEText(HTML_MSG, 'html', 'utf-8')
            msgAlternative.attach(czysty_tekst)
            msgAlternative.attach(czesc_html)

            fp = open('sign.png', 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            msgImage.add_header('Content-ID', '<sign.png>')
            msgRoot.attach(msgImage)

            user = "rest.monitor"
            pwd = "KmCg^9(9Sr"
            
            try:
                server = smtplib.SMTP("smtp.gmail.com", 587)
                server.ehlo()
                server.starttls()
                server.login(user, pwd)
                server.sendmail(FROM, RECIPIENTS, msgRoot.as_string())
                server.close()
                print 'Pomyślnie wysłano wiadomość.'
                return 0;
            except:
                print "Nie udało się wysłać wiadomości."
                return 1;
            return;

def check_services_status():
    with open(APP_SERVICESTATUS) as file:
        contentOfFile = file.read()

    configuration = ConfigParser.ConfigParser()
    configuration.read(APP_CONFIG)

    dt = datetime.datetime.now()
    delta = datetime.timedelta(seconds=int(configuration.get("TIMEOUT", 'mail')) )

    lines = contentOfFile.split('\n')
    contentOfFile = ""
    i = 0
    for line in lines:
        if i==0:
            i+=1
            pass
        elif not line == "":
            if len(mailSent)<i:
                mailSent.append(False)
            fields = line.split(' ')
            date = fields[2]+' '+fields[3]
            if datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f') < dt-delta:
                if mailSent[i-1]==False and (fields[1]=='RED:' or fields[1]=='YELLOW:'):
                    send_warning_status(fields[0], [configuration.get("MAIL_ADDR", 'mail')]);
                    mailSent[i-1]=True
            elif mailSent[i-1]==True and fields[1]=='GREEN:':
                mailSent[i-1]=False
            i+=1;
