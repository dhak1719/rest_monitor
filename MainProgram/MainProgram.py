from LoadConfiguration import *
from Functions import *
from sendMail import *


class MainProgram():

    def __init__(self):
        self.listOfActiveServices = getServicesList()
        
    def start(self):
        try:
            self.listOfActiveServices = config()
            while True:
                counter = 0.0
                self.listOfActiveServices = getServicesList()
                checkAllServices(self.listOfActiveServices)
                check_services_status()
                while True:
                    if counter >= float(loader.getTimeout('interval')):
                        break
                    else:
                        time.sleep(10.0)
                        counter += 10.0
        except KeyboardInterrupt:
            writeLog("PROGRAM", "Program closed. Exception - Keyboard Interrupt")
            print("\n\n###PROGRAM KEYBOARD INTERRUPT###\n\n")
        
    def ping(self, name):
        self.listOfActiveServices = getServicesList()
        writeLog(name, "Service " + name + " was pinged.")
        check = True
        for SearchingService in self.listOfActiveServices:
            if SearchingService.name == name:
                checkService(SearchingService)
                check = False
        if check:
            configNewService(name)

    def remove(self, name):
        self.listOfActiveServices = getServicesList()
        for SearchingService in self.listOfActiveServices:
            if SearchingService.name == name:
                self.listOfActiveServices.remove(SearchingService)
                writeLog(name, "Service " + name + " was removed from list od active services")
        removeServiceFromConfig(name)
        writeLog(name, "Service " + name + " was removed from config.txt")
        removeServiceFromServiceStatus(name)
        writeLog(name, "Service " + name + " was removed from ServiceStatus")

    def configure(self):
        self.listOfActiveServices = getServicesList()


if __name__ == "__main__":
    prog = MainProgram()
    prog.start()
