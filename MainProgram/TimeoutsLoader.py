import ConfigParser
import os


class TimeoutsLoader(object):
    def __init__(self):
        self.upload = 5
        self.queue = 10
        self.request = 300
        self.mail = 3600
        self.interval = 600

    def loadTimeouts(self):
        main_path = os.path.dirname(os.path.abspath(__file__))
        config_path = os.path.join(main_path, 'config.txt')

        configuration = ConfigParser.ConfigParser()
        configuration.read(config_path)

        self.upload = configuration.get("TIMEOUT", 'upload')
        self.queue = configuration.get("TIMEOUT", 'queue')
        self.request = configuration.get("TIMEOUT", 'request')
        self.mail = configuration.get("TIMEOUT", 'mail')
        self.interval = configuration.get("TIMEOUT", 'interval')

    def getTimeout(self, timeout_name):
        self.loadTimeouts()
        if timeout_name == 'upload':
            return self.upload
        elif timeout_name == 'queue':
            return self.queue
        elif timeout_name == 'request':
            return self.request
        elif timeout_name == 'mail':
            return self.mail
        elif timeout_name == 'interval':
            return self.interval
        else:
            return '-1'