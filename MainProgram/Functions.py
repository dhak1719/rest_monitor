__author__ = 'konrad'

import urllib2
import datetime
import time
import re
import multiprocessing
from TimeoutsLoader import *

url = "http://ws.clarin-pl.eu/nlprest/base"

APP_MAIN = os.path.dirname(os.path.abspath(__file__))
APP_SERVICESTATUS = os.path.join(APP_MAIN, 'ServiceStatus')
APP_SERVICES = os.path.join(APP_MAIN, 'Services')
APP_CONFIG = os.path.join(APP_MAIN, 'config.txt')
APP_LOG = os.path.join(APP_MAIN, 'log.txt')

timeoutsLoader = TimeoutsLoader()


def writeLog(servicename, description):
    if not os.path.exists(APP_LOG):
        with open(APP_LOG, 'a') as newLogFile:
            newLogFile.write("")
    with open(APP_LOG) as file:
        contentOfLogFile = file.read()
    dt = datetime.datetime.now()
    oldDate = datetime.datetime.now() - datetime.timedelta(days=31)
    try:
        lines = contentOfLogFile.split('\n')
        contentOfLogFile = ""
        for line in lines:
            if not line == "":
                fields = line.split('\t')
                date = fields[0]
                if (datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f') < oldDate):
                    pass
                else:
                    contentOfLogFile += (line + '\n')
    finally:
        with open(APP_LOG, 'w') as file:
            file.write(contentOfLogFile + str(dt) + "\t" + servicename + "\t" + description + "\n")


def writeStatus(servicename, color):
    with open(APP_SERVICESTATUS) as file:
        contentOfServiceStatus = file.read()
    dt = datetime.datetime.now()
    contentOfServiceStatus = re.sub("^....-..-.. ..:..:..\.[0-9]*", str(dt), contentOfServiceStatus)
    matchString = re.search(servicename, contentOfServiceStatus)
    if matchString is None:
        writeLog(servicename, "Added into file SERVICESTATUS")
        contentOfServiceStatus += (servicename + color + ": " + str(dt) + "\n")
    else:
        matchString = re.search(servicename + color, contentOfServiceStatus)
        if matchString is None:
            writeLog(servicename, "Change status color to " + str(color) + ".")
            contentOfServiceStatus = re.sub(servicename + ": " + "[A-Z]*" + ": " + "....-..-.. ..:..:..\.[0-9]*",
                                            servicename + color + ": " + str(dt), contentOfServiceStatus)
    f = open(APP_SERVICESTATUS, 'w')
    try:
        f.write(contentOfServiceStatus)
    finally:
        f.close()


def checkAllServices(listOfActiveServices):
    print("Running...")
    processes = [
        multiprocessing.Process(target=checkService, args=(objectOfService,), name=str(objectOfService.name))
        for objectOfService in listOfActiveServices]
    # Run processes
    for p in processes:
        p.daemon = False
        p.start()
        time.sleep(0.5)

    # Exit the completed processes
    for p in processes:
        p.join(float(timeoutsLoader.getTimeout('request')))
        if p.is_alive():
            writeLog(p.name, "Process not respoding in request's time.")
            writeStatus(p.name, ": RED")
        p.terminate()
        p.join()


def checkService(objectOfService):
    pathDir = os.path.join(APP_SERVICES, objectOfService.name)
    with open(pathDir + "/WE.file") as file2:
        data = file2.read()
    if data == "":
        configDefaultService(objectOfService)
    q = multiprocessing.Queue()
    p = multiprocessing.Process(target=objectOfService.tool(data, objectOfService.name, q, objectOfService.parameters),
                                name="tool")
    p.daemon = True
    p.start()
    p.join()
    if not p.is_alive():
        data, status = q.get()
        if status == "ERROR_QUEUE":
            # blad spowodowany przekroczeniem czasu kolejki oczekiwania 
            writeStatus(objectOfService.name, ": BLUE")
        elif status == "ERROR":
            # blad krytyczny w funkcji tool - szczegoly tam.
            writeStatus(objectOfService.name, ": RED")
        else:
            with open(pathDir + '/ID.file', "w") as idFile:
                idFile.write(data)
            try:
                content = urllib2.urlopen(urllib2.Request(url + '/downloadbin/' + data)).read()
                if not os.path.isfile(pathDir + '/WY.file'):
                    open(pathDir + '/WY.file', 'a').close()
                with open(pathDir + '/WY.file', "r") as outfile:
                    exampleData = outfile.read()
                compareResultOfService(content, exampleData, objectOfService.name)
            except:
                writeLog(objectOfService.name, "Error when downloading content from the server file")
                writeStatus(objectOfService.name, ": RED")
    q.close()
    p.terminate()
    p.join()


def compareResultOfService(newResult, oldResult, servicename):
    if oldResult == "":
        writeLog(servicename, "Default result was empty, it was replaced to the new result.")
        writeStatus(servicename, ": GREEN")
        pathFile = os.path.join(APP_SERVICES, servicename, 'WY.file')
        contentOfNewResult = open(pathFile, 'w')
        contentOfNewResult.write(newResult)
    else:
        if newResult == oldResult:
            writeStatus(servicename, ": GREEN")
        else:
            writeStatus(servicename, ": YELLOW")
            pathFile = os.path.join(APP_SERVICES, servicename, 'WY_new.file')
            contentOfNewResult = open(pathFile, 'w')
            contentOfNewResult.write(newResult)


def configDefaultService(objectOfService):
    servicename = objectOfService.name
    configuration = ConfigParser.ConfigParser()
    configuration.read(APP_CONFIG)
    boolRoute = False
    listSections = configuration.sections()
    for a in listSections:
        listItems = configuration.items(a)
        if a == "INPUT":
            for name, value in listItems:
                if name == servicename:
                    pathDir = os.path.join(APP_SERVICES, name)
                    with open(pathDir + '/WE.file.org', "w") as defaultFile:
                        defaultFile.write(value)
                    data = objectOfService.upload(pathDir + '/WE.file.org')
                    with open(pathDir + '/WE.file', "w") as retUpload:
                        retUpload.write(data)
                else:
                    boolRoute = True
        if a == "ROUTE" and boolRoute:
            for name, value in listItems:
                if name == servicename:
                    configRoute(objectOfService)


def configRoute(objectOfService):
    dirInFile = objectOfService.name
    dirOutFile = objectOfService.routeName
    compareInOutFiles(dirInFile, dirOutFile)


def compareInOutFiles(dirIn, dirOut):
    pathDirIn = os.path.join(APP_SERVICES, dirIn)
    pathDirOut = os.path.join(APP_SERVICES, dirOut)
    if not os.path.isfile(pathDirIn + '/WE.file'):
        open(pathDirIn + '/WE.file', 'a').close()
    if not os.path.isfile(pathDirOut + '/ID.file'):
        open(pathDirOut + '/ID.file', 'a').close()
    fileIn = open(pathDirIn + '/WE.file').read()
    fileOut = open(pathDirOut + '/ID.file').read()
    if not fileIn == fileOut:
        tmp = open(pathDirIn + '/WE.file', 'w')
        try:
            tmp.write(fileOut)
        finally:
            tmp.close()


def removeServiceFromServiceStatus(name):
    with open(APP_SERVICESTATUS) as file:
        contentOfServiceStatus = file.read()
    try:
        lines = contentOfServiceStatus.split('\n')
        contentOfServiceStatus = ""
        for line in lines:
            matchString = re.search(name, line)
            if matchString is None:
                contentOfServiceStatus += (line + '\n')
    finally:
        with open(APP_SERVICESTATUS, 'w') as file:
            file.write(contentOfServiceStatus + "\n")


def removeServiceFromConfig(name):
    with open(APP_CONFIG) as file:
        contentOfConfig = file.read()
    try:
        lines = contentOfConfig.split('\n')
        contentOfConfig = ""
        for line in lines:
            matchString = re.search(name, line)
            if matchString is None:
                contentOfConfig += (line + '\n')
    finally:
        with open(APP_CONFIG, 'w') as file:
            file.write(contentOfConfig + "\n")