__author__ = 'konrad'

import json
import time
from Functions import *

url = "http://ws.clarin-pl.eu/nlprest/base"


loader = TimeoutsLoader()


class Service(object):
    def __init__(self, servicename):
        self.name = servicename
        self.parameters = None
        self.routeName = None

    def upload(self, file):
        with open(file, "r") as myFile:
            doc = myFile.read()
            ret = ""
            try:
                ret = urllib2.urlopen(urllib2.Request(url + '/upload/', doc, {'Content-Type': 'text/plain'}),
                                      timeout=float(loader.getTimeout('upload'))).read()
                writeStatus("UPLOAD", ": GREEN")
            except Exception as inst:
                writeLog("upload", "Error in function UPDATE during request()/urlopen() function.")
                writeStatus("UPLOAD", ": RED")
        return ret

    def tool(self, fileId, toolname, queue, parameters):
        print parameters
        try:
            if parameters is None:
                parameters2 = {'model': 'default'}
            else:
                param = parameters.split(":")
                parameters2 = {param[0]: param[1].strip()}
            doc = json.dumps(parameters2).replace(' ', '%20')
            timer = 0
            try:
                taskId = urllib2.urlopen(urllib2.Request(url + '/startTask/' + toolname + '/' + fileId + '/' + doc)).read()
                resp = urllib2.urlopen(urllib2.Request(url + '/getStatus/' + taskId))
                data = json.load(resp)
            except:                
                raise Exception('urllib2')
            while data["status"] == "QUEUE" or data["status"] == "PROCESSING":
                time.sleep(0.2)
                timer += 1
                resp = urllib2.urlopen(urllib2.Request(url + '/getStatus/' + taskId))
                data = json.load(resp)
                if timer > (float(loader.getTimeout('queue')) * 5):
                    raise Exception('kolejka')
                if data["status"] == "ERROR":
                    raise Exception('status')
        except Exception as inst:
            nameOfExc = inst.args[0]
            if nameOfExc == 'kolejka':
                # blad kolejki - zbyt dlugie oczekiwanie na wykonanie zlecenia
                writeLog(toolname, "Queue time was exceeded.")
                queue.put(("", "ERROR"))
            elif nameOfExc == 'urllib2':
                # blad startTask/ getStatus
                writeLog(toolname, "Cannot start a task or check status. Host is unattainable")
                queue.put(("", "ERROR"))
            elif nameOfExc == 'status':
                # blad w otrzymanym statusie
                writeLog(toolname, "Received a ERROR status.")
                queue.put(("", "ERROR"))
            else:
                # inny blad.
                writeLog(toolname, "Other error in startTask/getStatus function.")
                queue.put(("", "ERROR"))
        else:
            queue.put((data["value"], data["status"]))

    def setParameter(self, param):
        self.parameters = param

    def setRouteName(self, rName):
        self.routeName = rName


